namespace ProSol.Html.Data;

internal enum TagKind
{
    Comment,
    Opening,
    Closing,
    Inline
}
